import {Component} from "react";

class CreateContent extends Component{
    render(){
        return(
            <article>
                <h1>Create</h1>
                <form action="/create_process" method="post"
                onSubmit={function(e){
                    e.preventDefault();
                    this.props.onCreateContent(
                        e.target.title.value,
                        e.target.content.value
                    );
                }.bind(this)}>
                   <p><input type="text" name="title" placeholder="title"/> </p>
                   <p><input type="text" name="content" placeholder="content"/></p>
                   <input type="submit"/>
                </form>
            </article>
        );
    }
}

export default CreateContent;