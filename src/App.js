import { Component } from 'react';
import TOC from "./components/TOC"
import Subject from "./components/Subject"
import ReadContent from "./components/ReadContent"
import CreateContent from "./components/CreateContent"
import Controll from "./components/Controll"

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			mode: 'create',
			selected_content_id: 2,
			subject: { title: 'WEB', sub: 'world wide web!' },
			welcome: { title: 'welcome', desc: 'Hello, React!!' },
			contents: [
				{ id: 1, title: 'HTML', desc: 'HTML is for infomation' },
				{ id: 2, title: 'CSS', desc: 'css is for infomation' },
				{ id: 3, title: 'JavaScript', desc: 'JavaScript is for infomation' }
			]
		}
	}
	render() {
		var _title, _desc, _article = null;
		if (this.state.mode === 'welcome') {
			_title = this.state.welcome.title;
			_desc = this.state.welcome.desc;
			_article = <ReadContent title={_title} desc={_desc} />
		} else if (this.state.mode === 'read') {
			var i = 0;
			while (i < this.state.contents.length) {
				var data = this.state.contents[i];
				if (data.id === this.state.selected_content_id) {
					_title = data.title;
					_desc = data.desc;
					break;
				}
				i += 1;
			}
			_article = <ReadContent title={_title} desc={_desc} />
		} else if (this.state.mode === 'create') {
			_article =
				<CreateContent
					onCreateContent={function(_title,_desc){
						this.state.contents.push(
							{
								id : this.state.contents.length,
								title : _title,
								desc : _desc
							})
						this.setState({contents:this.state.contents});	
					}.bind(this)}
					/>
		}
		return (
			<div>
				<Subject
					title={this.state.subject.title}
					sub={this.state.subject.sub}
					onChangePage={function () {
						this.setState({ mode: 'welcome' });
					}.bind(this)}
				/>
				<TOC onChangePage={function (id) {
					this.setState({ mode: 'read', selected_content_id: Number(id) })
				}.bind(this)} data={this.state.contents} />
				<Controll
					onChangeMode={function (_mode) {
						this.setState({ mode: _mode })
					}.bind(this)}
				/>

				{_article}
			</div>
		);
	}
}

export default App;
